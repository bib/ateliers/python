#+TITLE: Ateliers Python @ BIB (2017-2018) - #4
#+SUBTITLE: Comment représenter des données hiérarchiques ?
#+LANGUAGE: fr
#+AUTHOR: Hugo Geoffroy "pistache"
#+DATE: <2018-01-05 ven.>
#+LATEX_HEADER: \usepackage[margin=0.5in]{geometry}
* Introduction

  En théorie des graphes, un arbre est un *graphe* *non orienté*, *acyclique* et
  *connexe*, dont la forme est analogue à celle d'un arbre.

  [[file:atelier-0004-arbre.png][Arbre]] 

  Les *noeuds* qui n'ont pas *d'ascendants* (les plus bas dans l'image) sont les
  *racines*, un *arbre* qui ne possède qu'une seule *racine* est un arbre
  *enraciné*. Les *noeuds* qui n'ont pas *de descendants* (les plus hauts dans
  l'image) sont les feuilles.

  Au cours du premier atelier, nous avons observé un premier type de structure
  hiérarchique : les grammaires, composées d'éléments des début (les racines) et
  d'élements terminaux (les feuilles). Les grammaires ne sont pas cependant pas
  des arbres au sens strict, car elles peuvent contenir des boucles, qui leur
  font perdre la caractéristique *d'acyclique*. Les graphes, et leur forme plus
  spécifique les arbres, sont très utilisés en tant que structure de données,
  pour représenter un programme, ou n'importe quelle structure arborescente.

  Chaque jour, en utilisant un ordinateur, nous utilisons une structure
  aborescente : les systèmes de fichiers de nos ordinateurs. Chaque dossier peut
  contenir d'autres dossiers (les "noeuds"), ou des fichiers (les "feuilles").
  Sous Windows, il y a plusieurs racines à la structure de fichiers ("C:", "D:",
  etc), tandis que sous les systèmes basés sur UNIX, il n'y a qu'une seule
  racine ("/").

* Jette-mots
  - Graphe, arbre, cycle
  - Noeud, racine, feuille
