            ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
                 ATELIERS PYTHON @ BIB (2017-2018) - #3
             Comment représenter des données structurées ?

                        Hugo Geoffroy "pistache"
            ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━


                           <2017-11-17 ven.>


Table des matières
──────────────────

1 Introduction
2 Jette-mots
3 Types primitifs
.. 3.1 Nombres intégraux et réels
..... 3.1.1 Introduction et questions
..... 3.1.2 Entiers
..... 3.1.3 Booléens
..... 3.1.4 Nombres flottants et complexes
..... 3.1.5 Fractions et nombres décimaux
..... 3.1.6 Synthèse
.. 3.2 Chaînes de caractères et d'octets
..... 3.2.1 Introduction
..... 3.2.2 Chaînes de caractères
..... 3.2.3 Chaînes d'octets
..... 3.2.4 Synthèse
.. 3.3 Conteneurs de données
..... 3.3.1 Introduction
..... 3.3.2 Listes
..... 3.3.3 Tuples
..... 3.3.4 Dictionnaires
..... 3.3.5 Ensembles
..... 3.3.6 Synthèse
4 Annexe
.. 4.1 Sources et références


1 Introduction
══════════════

  Comme nous l'avons vu lors du dernier atelier, nous pouvons établir
  une distinction entre les "données" et le "programme" qu'on applique à
  ces données. Bien sur, cette distinction est floue : le programme peut
  lui être exprimé sous la forme de données ; comme nous le montre
  l'universalité des machines de Turing. Plusieurs techniques de
  programmation se basent d'ailleurs se le fait de modifier le programme
  pendant son exécution, en le traitant comme des données, comme par
  exemple la "méta-programmation".

  Les données sont des ensembles de valeurs, une donnée est une "valeur
  donnée", stockée à un certain emplacement. Il nous arrive de stocker
  des données qui nous fournissent des informations additionelles sur
  d'autres données, nous les appellerons alors "méta-données".

  Toute les données nécessitent interprétation avant de pouvoir devenir
  des informations, cette interprétation est faite selon une structure
  précise. Bien sur, tout ceci est parfaitement analogue au sujet du
  premier atelier, et cette structure représente un "langage" dans
  lequel nous avons exprimé nos données, bien que ce ne soit pas
  forcèment le même type de langage dans la hiérarchie de Chomsky.

  Il existe toujours plusieurs représentations pour un certain ensemble
  d'informations, et notre choix de représentation a d'importantes
  conséquences sur la structure et la complexité de notre programme :
  nous l'avons vu avec la machine de Turing et l'additioneur, qui aurait
  été bien plus complexe à écrire en binaire. Bien sur, ces choix ont
  aussi d'importantes conséquences sur les caractéristiques de
  performances de notre programme, tant en nombre d'opérations qu'en
  espace utilisé.


2 Jette-mots
════════════

  • Données, valeur, information, structure
  • Représentation, interprétation
  • Complexité, performance
  • Nombre, texte, fichier
  • Liste, dictionnaire , ensemble


3 Types primitifs
═════════════════

  [Hiérarchie des types de base]


[Hiérarchie des types de base]
file:atelier-0003-hierarchie-types-small.png

3.1 Nombres intégraux et réels
──────────────────────────────

3.1.1 Introduction et questions
╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌

  Python nous fournit des types distincts pour exprimer toutes ces
  formes de nombres :
  • Booléens (`bool')
  • Entiers (`int')
  • Flottants (`float')
  • Décimaux (`decimal.Decimal')
  • Fractions (`fractions.Fraction')

  Certains types peuvent aussi se comporter comme une fonction : cette
  fonction est alors un "constructeur", permettant de construire une
  valeur de ce type à partir d'une valeur d'un autre type. La plupart
  des types peuvent être construits à partir de leur représentation
  textuelle, donc à partir d'une valeur de type `str'.

  La fonction `type' nous permet d'obtenir le type d'une valeur. Notez
  que `type' est aussi un `type', et que le type de tous les types est…
  `type'.

  Pour obtenir le type d'une valeur, on peut utiliser la fonction
  `type'. Notez que `type' est aussi un type, mais qu'il n'est pas
  utilisé

  *Question :* en quoi un "booléen" est-il un nombre ? Question :*
  *lesquels de ces types nous permettent de représenter une valeur
  *continue ? lesquels sont restreints à des valeurs discrètes ?
  *Question :* est-ce qu'un de ces types est borné ? Question :* que
  *donne le résultat de *True + True*, ou *False / False* ?


3.1.2 Entiers
╌╌╌╌╌╌╌╌╌╌╌╌╌

  Les nombres entiers de Python ne sont pas bornés : cela veut dire que
  leur seule limite est la capacité de stockage, et que leur
  représentation n'est pas fixe. L'ensemble des nombres entiers est
  naturellement discret : dans un intervalle fini, le nombre d'élements
  sera fini, contrairement à par exemple l'ensemble des nombres réels,
  dans lequel un intervalle fini contient une quantité infinie de
  nombres.


◊ 3.1.2.1 Nombres entiers de taille fixe

  Dans d'autres langages (comme C), les nombres entiers sont stockés en
  utilisant une représentation à taille fixe, avec un certain nombre de
  bits utilisés pour représenter le nombre (souvent des puissances de
  deux), et avec un bit potentiellement reservé pour le signe. En
  Python, ce n'est pas le cas pour les `int' (même si c'est bien le cas
  pour les `float').

  Ces représentations fixes sont plus efficaces, mais moins souples à
  l'utilisation, et introduisent une source de complexité dans la
  gestion des erreurs de bornes.


3.1.3 Booléens
╌╌╌╌╌╌╌╌╌╌╌╌╌╌

  En Python, les booléens sont des nombres : le type "bool" est une
  "sous-classe" du type "int", ce qui veut dire que tous les booléens
  sont des nombres, mais que tous les nombres ne sont pas des booléens ;
  les booléens appartiennent à l'ensemble des nombres. Il n'y a
  d'ailleurs que deux de ces nombres qui appartiennent à l'ensemble des
  booléens : `1' et `0', représentant `True' et `False'.

  `1' et `True' représentent donc la même "valeur", mais avec un "type"
  différent : cela forme une "donnée" différente, car elle devra être
  interprétée avec d'autres règles. Il faut faire attention aux
  relations : `1' n'est pas un booléen, mais `True' est bien un nombre,
  même s'il est aussi un booléen.

  Cela nous introduit à la notion de "hiérarchie de types" : les types
  peuvent être des spécialisation d'un type, de ses règles
  d'interprétation et des comportements qu'on peut appliquer à ses
  valeurs. Ces spécialisations sont appelées des "sous-classes" : les
  valeurs de ces types sont appelées des "instances".

  Si on a un type A, sous-classé par un type B, les instances de A ne
  sont *pas* des instances de B, mais les instances de B sont *aussi*
  des instances de A.

  Dans le cas de `bool' et `int', `True' est une instance de `bool', et
  donc par extension une instance de `int', mais `1' n'est qu'une
  instance de `int', ce n'est pas une instance de `bool'. Ce principe
  est important, et dans le cas des booléens ne saute pas directement
  aux yeux, car `1' pourra aussi être utilisé comme un booléen, même si
  ce n'en est pas un.


3.1.4 Nombres flottants et complexes
╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌

  La notion de "nombre flottant" est ambiguë, selon le contexte ou nous
  nous trouvons. Il existe plusieurs systèmes de "représentation à
  virgule flottante" des nombres, qui n'ont pas tous les mêmes
  caractéristiques, et fondamentalement, un "nombre à virgule flottante"
  n'est pas différent d'un "nombre décimal". Cependant, en informatique,
  l'expression "nombre flottant" fait référence à un standard, au
  standard IEEE 754, établi en 1985. Cet standard est un très bel outil,
  d'une grande complexité mais aussi assez simple d'utilisation et très
  flexible. Quand on ne connaît pas bien les implications de la
  représentation utilisée, beaucoup de surprises sont possibles, et la
  complexité que cela peut créer a déjà été source de bien des
  cauchemars ! Dans ce standard, il n'est pas possible d'exprimer tous
  les nombres de l'ensemble que nous représentons symboliquement : on
  s'attendrait à ce qu'un "nombre flottant" puisse représenter tout
  nombre décimal, mais ce n'est pas le cas !

  En effet, bien que `-0.5' soit un nombre flottant, `0.1' ne l'est pas,
  et `2j+2**53+1' n'est pas un nombre complexe valide. Pourtant :

  ┌────
  │ >>> 0.1
  │ 0.1
  │ >>> type(0.1)
  │ <class 'float'>
  └────

  Ces résultats auraient tendance à nous faire croire le contraire.
  Cependant, Python ne nous montre pas tout :

  ┌────
  │ >>> '{:.64f}'.format(0.1)
  │ '0.1000000000000000055511151231257827021181583404541015625000000000'
  └────

  En demandant d'afficher `0.1' avec 64 nombres après la virgule, nous
  observons que nous n'avons pas exactement `0.1'. Ce nombre que nous
  voyons est le nombre flottant *le plus proche* de `0.1'. La même
  expérience ne donne pas le même résultat avec `0.5' :

  ┌────
  │ >>> '{:.64f}'.format(0.5)
  │ '0.5000000000000000000000000000000000000000000000000000000000000000'
  └────

  `0.5' est bien un nombre flottant. La différence entre `0.5' et `0.1',
  c'est que l'un (`0.5') peut être exprimé comme une somme d'inverses de
  puissances de 2, alors que l'autre (`0.1') ne peut être écrit sous
  cette forme, et doit être approximé : la représentation des nombres
  flottante est une représentation discrète, bornée, utilisant les
  inverses de puissance de deux.

  Cela explique aussi pourquoi `2j+2**53+1' n'est pas un nombre complexe
  : les nombres complexes sont composés de deux parties, réelles et
  imaginaires, toutes les deux flottantes, et `2.0**53+1' n'est pas un
  nombre flottant existant, `2**53' étant le nombre flottant le plus
  proche :

  ┌────
  │ >>> 2j+2**53
  │ (9007199254740992+2j)
  │ '0.5000000000000000000000000000000000000000000000000000000000000000'
  │ >>> 2j+2**53+1
  │ (9007199254740992+2j)
  └────

  La représentation flottante est donc bornée (elle a une valeur maximum
  et minimum) et discrète (le nombre de valeurs dans un intervalle fini
  y est lui aussi fini), ce qui étend ces caractéristiques à la
  représentation des nombres complexes.


3.1.5 Fractions et nombres décimaux
╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌

  Quand nous écrivons `0.1' en Python, nous écrivons un nombre flottant,
  et comme nous l'avons-vu, Python utilisera le nombre flottant le plus
  proche de la valeur que nous avons demandé. De la même manière, quand
  nous écrivons `2/3', nous écrivons une opération de divison, et le
  résultat sera le nombre le plus proche correspondant au résultat de la
  division. Ici le résultat ne serait même pas un nombre décimal, même
  si ce serait bien un nombre réel.

  Python nous fournit aussi des types pour représenter les valeurs
  précises dans ces deux cas :
  • `fractions.Fraction` : permettant de représenter n'importe quel réel
    sous la forme de deux nombres entiers
  • `decimal.Decimal` : permettant de représenter n'importe quel nombre
    décimal

  Ces deux types peuvent aussi être utilisés dans des opérations
  mathématiques, tant qu'ils le sont avec des types compatibles,
  potentiellement convertis d'un autre type. Evidemment, ces types sont
  plus lents que `int', `float', dont la représentation permet
  d'effectuer des opérations de manière très efficace.

  `decimal.Decimal' est souvent utilisé avec un nombre décimal sous la
  forme de texte :

  ┌────
  │ >>> import decimal
  │ >>> decimal.Decimal("0.1")
  │ Decimal('0.1')
  └────

  Attention à ne pas donner un argument flottant à `Decimal', le nombre
  serait formé d'après l'approximation en nombre flottant :

  ┌────
  │ >>> import decimal
  │ >>> decimal.Decimal(0.1)
  │ Decimal('0.1000000000000000055511151231257827021181583404541015625')
  └────

  `fractions.Fraction' est lui utilisé avec deux arguments, tous les
  deux des nombres entiers :

  ┌────
  │ >>> import fractions
  │ >>> fractions.Fractions(2,3)
  │ Fraction(2, 3)
  └────

  De la même manière qu'avec `decimal.Decimal', `Fraction' donne un
  résultat complètement différent avec un nombre flottant, bien
  qu'approximativement équivalent :

  ┌────
  │ >>> import fractions
  │ >>> fractions.Fractions(2/3)
  │ Fraction(6004799503160661, 9007199254740992)
  └────

  Cette fraction est l'approximation du résultat de `2/3' en nombre
  flottant.

  La représentation fractionnaire n'est donc pas bornée (car composée de
  `int'), et n'est pas discrète, car il y a une quantité infinie de
  fractions différentes dans tout interval fini et non-nul.

  La représentation décimale est elle bornée et discrète : la précision
  et l'intervalle y sont déterminés, même si ils peuvent être changés.


3.1.6 Synthèse
╌╌╌╌╌╌╌╌╌╌╌╌╌╌

  ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
   Nom       Type Python         Discret  Borné  Exemples          Contre-exemples 
  ─────────────────────────────────────────────────────────────────────────────────
   Booléen   bool                X        X      True              0, 1, 2         
   Entier    int                 X               0, 2**1024, True  2.0, None       
   Flottant  float               X        X      -0.5, 2.0**1023   0.1, 2.0**1024  
   Complexe  complex             X        X      1j,               2j+2**53+1      
   Décimal   decimal.Decimal     X        X      Decimal("0.1")    0.1             
   Fraction  fractions.Fraction                  Fraction(2, 3)    2/3             
  ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━


3.2 Chaînes de caractères et d'octets
─────────────────────────────────────

3.2.1 Introduction
╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌

  Python nous fournit une manière simplifiée de représenter de chaînes
  de caractères (avec `str') et d'octets (avec `bytes'). En Python 3, un
  caractère est un symbole Unicode, et une chaine de caractères est une
  séquence valide de symboles unicodes, qui a plusieurs représentations
  en octets, selon l'encodage utilisé. Un de ces encodages est UTF-8,
  c'est celui le plus couramment utilisé aujourd'hui (il y a aussi
  ISO-8859-15, qu'on retrouve sous Windows).

  Une chaine d'octets peut représenter une chaine de caractères, mais
  aussi n'importe quelle séquence d'octets, comme un fichier binaire ou
  du contenu chiffré.


3.2.2 Chaînes de caractères
╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌

  Pour définir une chaîne de caractères, on utilise simplement les
  guillemets, simples ou doubles, mais on peut aussi utiliser le
  constructeur pour convertir une autre valeur :

  ┌────
  │ >>> x = "foo"
  │ >>> y = str(4)
  │ >>> type(x)
  │ <class 'str'>
  │ >>> type(y)
  │ <class 'str'>
  │ >>> x
  │ 'foo'
  │ >>> y
  │ '4'
  └────


3.2.3 Chaînes d'octets
╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌

  On peut définir une chaîne d'octets de plusieurs manières, soit avec
  une liste de nombres (de `0' à `255'), qui représentent chaque octet,
  ou avec la notation ASCII :

  ┌────
  │ >>> x = b"foo"
  │ >>> y = bytes([97,98,99])
  │ >>> type(x)
  │ <class 'bytes'>
  │ >>> type(y)
  │ <class 'bytes'>
  │ >>> x
  │ b'foo'
  │ >>> y
  │ b'abc'
  │ >>> list(x)
  │ [102, 111, 111]
  │ >>> list(y)
  │ [97, 98, 99]
  └────

  Notez que la représentation des bytes utilise la notation ASCII.

  Les chaînes de caractères ont la particularité que leurs sous-élements
  sont eux aussi des chaînes de caractères, contrairement aux chaînes
  d'octets, dont les élements sont retournés sous la forme d'entiers :

  ┌────
  │ >>> x = "foo"
  │ >>> y = b"bar"
  │ >>> x[0]
  │ "f"
  │ >>> y[0]
  │ 98
  └────


3.2.4 Synthèse
╌╌╌╌╌╌╌╌╌╌╌╌╌╌

  Ces deux types sont intimement liés, et représentent deux manières de
  représenter les mêmes valeurs : le type `str' opère sur la
  représentation en symboles Unicode, tandis `bytes' opère directement
  sur les octets.

  Il est possible d'encoder/décoder des chaînes de caractères Unicode en
  octets :

  ┌────
  │ >>> x = "Les serpents ça siffle !"
  │ >>> y = x.encode()
  │ >>> y
  │ b'Les serpents \xc3\xa7a siffle !'
  │ >>> y.decode()
  │ 'Les serpents ça siffle !'
  └────


3.3 Conteneurs de données
─────────────────────────

3.3.1 Introduction
╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌

  Les objets de certains types contiennent des références vers d'autres
  objets, ces types sont appelés des conteneurs, comme les :
  • Listes (`list')
  • Tuples (`tuple')
  • Dictionnaires (`dict')
  • Ensembles (`set')


3.3.2 Listes
╌╌╌╌╌╌╌╌╌╌╌╌

  Les listes sont des piles ordonnées, stockées en "Dernier entré,
  premier sorti" :

  ┌────
  │ >>> x = [1,2,3]
  │ >>> type(x)
  │ <class 'list'>
  │ >>> x
  │ [1, 2, 3]
  │ >>> x[0]
  │ 1
  │ >>> x[-1]
  │ 3
  │ >>> x[1:]
  │ (2, 3)
  │ >>> x.append(4)
  │ >>> x
  │ [1, 2, 3, 4]
  │ >>> del x[0]
  │ >>> x
  │ [2, 3, 4]
  │ >>> x.remove(3)
  │ >>> x
  │ [2, 4]
  │ >>> len(x) 
  │ 2
  └────


3.3.3 Tuples
╌╌╌╌╌╌╌╌╌╌╌╌

  Les tuples sont des séquences immutables et ordonnées :

  ┌────
  │ >>> x = (1,2,3)
  │ >>> type(x)
  │ <class 'tuple'>
  │ >>> x
  │ (1, 2, 3)
  │ >>> x[0]
  │ 1
  │ >>> x[-1]
  │ 3
  │ >>> x[1:]
  │ (2, 3)
  │ >>> len(x) 
  │ 2
  └────


3.3.4 Dictionnaires
╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌

  Les dictionnaires sont des associations "clefs-valeur", non ordonnées
  :

  ┌────
  │ >>> x = {1: "un", 2: "deux", 3: "trois"}
  │ >>> type(x)
  │ <class 'dict'>
  │ >>> x
  │ {1: 'un', 2: 'deux', 3: 'trois'}
  │ >>> x[1]
  │ 'un'
  │ >>> x[4] = 'quatre'
  │ >>> x
  │ {1: 'un', 2: 'deux', 3: 'trois', 4: 'quatre'}
  │ >>> del x[2]
  │ >>> x
  │ {1: 'un', 3: 'trois', 4: 'quatre'}
  │ >>> len(x) 
  │ 3
  └────


3.3.5 Ensembles
╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌

  Les ensembles sont des collections d'objets non ordonnées, un peu
  analogues à des dictionnaires sans valeurs :

  ┌────
  │ >>> x = {"foo","bar","spam"}
  │ >>> type(x)
  │ <class 'set'>
  │ >>> x
  │ {'bar','spam', 'foo'}
  │ >>> x.add('egg')
  │ >>> x
  │ {'bar','spam', 'egg', 'foo'}
  │ >>> x.remove('bar')
  │ >>> x
  │ {'spam', 'egg', 'foo'}
  │ >>> len(x) 
  │ 3
  └────


3.3.6 Synthèse
╌╌╌╌╌╌╌╌╌╌╌╌╌╌

  ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
   Nom           Type     Mutable  Séquence  Association 
  ───────────────────────────────────────────────────────
   Liste         `list'   Oui      Oui       Non         
   Tuple         `tuple'  Non      Oui       Non         
   Dictionnaire  `dict'   Oui      Non       Oui         
   Ensemble      `set'    Oui      Non       Non         
  ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━


4 Annexe
════════

4.1 Sources et références
─────────────────────────

  • [{Documentation Python} Modèle de données]
  • [{Wikipedia} Pile (informatique)]
  • [{Wikipedia} Ensemble]


[{Documentation Python} Modèle de données]
https://docs.python.org/3/reference/datamodel.html

[{Wikipedia} Pile (informatique)]
https://fr.wikipedia.org/wiki/Pile_(informatique)

[{Wikipedia} Ensemble] https://fr.wikipedia.org/wiki/Ensemble
